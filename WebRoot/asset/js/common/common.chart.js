//$container;

function showLoading() {
	$('#over').show();
	$('#layout').show();
}

function hideLoading() {
	$('#over').hide();
	$('#layout').hide();
}

function initDeleteDialog(callback) {
	$('#confirm-dialog').dialog({
		hide : true, // 点击关闭是隐藏,如果不加这项,关闭弹窗后再点就会出错.
		autoOpen : false,
		height : 200,
		width : 300,
		title : 'Confirm',
		buttons : {
			'Yes' : function() {
				callback.call();
				$(this).dialog("close");
			},
			'No' : function() {
				$(this).dialog("close");
			}
		}
	});

	$('#confirm-dialog').dialog('open');
}

function initFromTo(){
	var time = $('#time_select').val();
	$('#from_select').hide();
	$('#to_select').hide();
	$('#from').hide();
	$('#to').hide();
	$('#from_month_select').hide();
	$('#to_month_select').hide();
	if(time == "daily"){
		$('#from').show();
		$('#to').show();
	}else if(time == "weekly"){
		$('#from_select').show();
		$('#to_select').show();
	}else{
		$('#from_month_select').show();
		$('#to_month_select').show();
	}
}

function drawChart(tdClass, title, ytitle){
	var spans = $('span.date_span');
	var dates = new Array(spans.length);
	for(var i = 0; i < spans.length; i ++){
		dates[i] = spans[i].getAttribute('data-prop');
	}
	var tds = $('td.' + tdClass);
	var dataArray = new Array(tds.length);
	for(var i = 0; i < tds.length; i++){
		dataArray[i] = parseInt(tds[i].innerHTML);
	}
	
	dates = dates.reverse();
	dataArray = dataArray.reverse();
	
	if(!ytitle || ytitle == ''){
		ytitle = 'total';
	}
	
	$('#container').highcharts({
        title: {
            text: title,
            x: -20 //center
        },
        xAxis: {
        	title: {
                text: '时间'
            },
            categories: dates
        },
        yAxis: {
            title: {
                text: title
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' chips'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: ytitle,
            data: dataArray
        }]
    });
}

// main+sub class 的值多线
function drawMutiCharts(title, mainClass, subClass){
	var spans = $('span.date_span');
	var dates = new Array(spans.length);
	for(var i = 0; i < spans.length; i ++){
		dates[spans.length - i - 1] = spans[i].getAttribute('data-prop');
	}
	
	var series = new Array(mainClass.length);
	if(!subClass || subClass.length == 0){
		subClass = ['nosub'];
	}
	
	for(var k = 0; k < mainClass.length; k ++){
		var subTitle = $('td[data-class="' + mainClass[k] +'"]')[0].innerHTML;
		var subData = new Array(mainClass.length);
		for(var i = 0; i < subClass.length; i ++){
			var subTds = $('td.' + mainClass[k] + subClass[i]);
			if(subTds.length == 0){
				subTds = $('td.' + mainClass[k]);
				i = subClass.length;
			}else if(subTds.length == 1){
				subTitle += $('td[data-class="' + mainClass[k] + subClass[i] +'"]')[0].innerHTML;
			}
			for(var j = 0; j < subTds.length; j ++){
				if(subData[subTds.length - j - 1]){
					subData[subTds.length - j - 1] += parseInt(subTds[j].innerHTML);
				}else{
					subData[subTds.length - j - 1] = parseInt(subTds[j].innerHTML);
				}
			}
		}
		series[k] = {
            name: subTitle,
            data: subData
        };
	}
	
	$('#container').highcharts({
        title: {
            text: title,
            x: -20 //center
        },
        xAxis: {
            categories: dates
        },
        yAxis: {
            title: {
                text: 'Chips'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 1
        },
        series: series
    });
}

// 总信息柱状加多线
function drawComplexTotalCharts(title, totalClass, dataClass){
	var spans = $('span.date_span');
	var dates = new Array(spans.length);
	for(var i = 0; i < spans.length; i ++){
		dates[spans.length - i - 1] = spans[i].getAttribute('data-prop');
	}
	var tds = $('td.total');
	if(totalClass && totalClass != ''){
		tds =  $('td.' + totalClass);
	}
	var totalData = new Array(tds.length);
	for(var i = 0; i < tds.length; i++){
		totalData[tds.length - i - 1] = parseInt(tds[i].innerHTML);
	}
	
	var subTitles = $('td.chart_title');
	if(dataClass && dataClass.length > 0){
		subTitles = new Array(dataClass.length);
		for(var i = 0; i < dataClass.length; i++){
			subTitles[i] = $('td[data-class="' + dataClass[i] +'"]')[0];
		}
	}
	
	var series = new Array(subTitles.length + 1);
	series[0] = {
		name : 'Total',
		type : 'column',
		data : totalData,
	};
	
	for(var i = 0; i < subTitles.length; i++){
		var subTds = $('td.' + subTitles[i].getAttribute('data-class'));
		var subData = new Array(subTds.length);
		for(var j = 0; j < subTds.length; j++){
			subData[subTds.length - j - 1] = parseInt(subTds[j].innerHTML);
		}
		var subTitle = subTitles[i].innerHTML.replace('<br>', '');
		series[i + 1] = {
            name: subTitle,
            type: 'spline',
            data: subData,
            marker: {
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[i + 1],
                fillColor: 'white'
            }
        };
	}
	
	$('#container').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: title
        },
        xAxis: [{
        	title: {
                text: 'Date'
            },
            categories: dates
        }],
        yAxis: [{
            labels: {
                format: '{value}',
            },
            title: {
                text: 'Chips',
            }
        }],
        tooltip: {
            shared: true
        },
        series:series,
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 1
        }
    });
}