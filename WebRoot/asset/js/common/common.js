$(document).ready(function() {
});

function enterToClick(event, btn_id) {
	event = event || window.event; 
    if(event.keyCode == 13)
    {
    	$("#" + btn_id).click();
    }
}

function showSuccessMsg(msg) {
	$.messager.show({
		msg:msg,
		width:400,
		height:60,
		timeout:2000,
		showType:'slide',
		style:{
			right:'',
			top:document.body.scrollTop+document.documentElement.scrollTop,
			bottom:''
		}
	});
}