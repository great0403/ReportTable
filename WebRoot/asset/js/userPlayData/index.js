$(document).ready(function() {
	$('#detail_dialog').dialog({
		title : 'userPlayData Detail - Edit',
		top : 30,
		width : 640,
		height : 650,
		closed : true,
		cache : false,
		modal : true,
		resizable : true
	});

	$('#add_dialog').dialog({
		title : 'userPlayData Detail - New userPlayData',
		top : 30,
		width : 640,
		height : 650,
		closed : true,
		cache : false,
		modal : true,
		resizable : true
	});
});

function searchBaseInfo() {
	var searchUserId = $('#searchUserId').val();	
	var searchDate = $('#date').val();
	location.href = 'userPlayData?&userId=' + searchUserId + '&date=' + date ;
}

function showDetail(rowData){
	if(rowData && rowData.id) {
	//	$('#detail_content').panel('refresh', 'fbActivity/detail/'+rowData.id);
		$('#detail_content').panel({
			href : 'fbActivity/detail/' + rowData.id,
		});
		$('#detail_dialog').dialog({    
		    title: 'FBActivity Detail - ' + rowData.id,
		    top : 30,
		    width: 500,    
		    height: 400,    
		    closed: true,    
		    cache: false,    
		    modal: true,
		    resizable: true
		});
		$('#detail_dialog').dialog('open');
	}
	
}

function edit(){
	$('.view-tag').hide();
	$('.edit-tag').show();
	$('#successMsg').html('');
	$('#errorMsg').html('');
}

function cancel(){
	$('.edit-tag').hide();
	$('.view-tag').show();
	$('#successMsg').html('');
	$('#errorMsg').html('');
}

function save(form_id){
	var date = $("input[name='date']").val();
	$("#"+form_id).form('submit', {
        url: 'userPlayData/save',
        onSubmit: function () {
        	if($("#"+form_id).form('validate')){
        		return compareTime(startTime, endTime);
        	}
        	return false;
        },
        success: function (data) {
        	    if(0 != data && -1!=data){
	        	    	var showMsg = 'Success';
	        	}else{
	        	    	var showMsg = 'Fail';        	    	
	        	}
	        	$.messager.show({
					msg:showMsg,
					width:400,
					height:60,
					timeout:2000,
					showType:'slide',
					style:{
						right:'',
						top:document.body.scrollTop+document.documentElement.scrollTop,
						bottom:''
					}
				});
        	   $('#detail_content').panel('refresh');
        	   $('#content').panel('refresh', 'userPlayData/table?page='+curr);
        	   if(totalNum != -1){
        		   $('#pp').pagination('refresh',{total: totalNum+1});
        	   }
	        	   
         }
    });
}

//判断日期，时间大小  
function compareTime(startDate, endDate) {   
  if (startDate.length > 0 && endDate.length > 0) {   
    var startDateTemp = startDate.split(" ");   
    var endDateTemp = endDate.split(" ");   

    var arrStartDate = startDateTemp[0].split("-");   
    var arrEndDate = endDateTemp[0].split("-");   

    var arrStartTime = startDateTemp[1].split(":");   
    var arrEndTime = endDateTemp[1].split(":");   

	var allStartDate = new Date(arrStartDate[0], arrStartDate[1], arrStartDate[2], arrStartTime[0], arrStartTime[1], arrStartTime[2]);   
	var allEndDate = new Date(arrEndDate[0], arrEndDate[1], arrEndDate[2], arrEndTime[0], arrEndTime[1], arrEndTime[2]);   

		if (allStartDate.getTime() >= allEndDate.getTime()) {   
		        return false;   
		} else {   
		    return true;   
		}   
  } 
}   
