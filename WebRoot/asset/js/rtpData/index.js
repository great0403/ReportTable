$(document).ready(function() {
	$('#detail_dialog').dialog({
		title : 'RtpData Detail - Edit',
		top : 30,
		width : 640,
		height : 650,
		closed : true,
		cache : false,
		modal : true,
		resizable : true
	});

	$('#add_dialog').dialog({
		title : 'RtpData Detail - New maChineData',
		top : 30,
		width : 640,
		height : 650,
		closed : true,
		cache : false,
		modal : true,
		resizable : true
	});
});

function searchBaseInfo() {
	var searchMachine = $('#searchMachine').val();
	var searchDate = $('#searchDate').val();
	
	location.href = 'rtpData? machine=' + searchMachine + '&date=' + searchDate;
}

function machineType(rec) {
	if(rec && rec.value){
		location.href = 'rtpData?machine=' + rec.value;
	}
}