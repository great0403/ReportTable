$(document).ready(function() {
	$('#detail_dialog').dialog({
		title : 'DailyData Detail - Edit',
		top : 30,
		width : 640,
		height : 650,
		closed : true,
		cache : false,
		modal : true,
		resizable : true
	});

	$('#add_dialog').dialog({
		title : 'DailyData Detail - New dailyData',
		top : 30,
		width : 640,
		height : 650,
		closed : true,
		cache : false,
		modal : true,
		resizable : true
	});
});

function searchBaseInfo() {
	var searchCountry = $('#searchCountry').val();
	var searchVersion = $('#searchVersion').val();
	var searchDate = $('#searchDate').val();
	
	location.href = 'dailyData?country=' + searchCountry + '&version=' + searchVersion + '&date=' + searchDate;
}

function countryType(rec) {
	if(rec && rec.value){
		location.href = 'dailyData?country=' + rec.value;
	}
}