package com.aemobile.casino.management.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.aemobile.casino.management.domain.RtpDataPo;

public interface RtpDataDao extends PagingAndSortingRepository<RtpDataPo, Long> {

	Page<RtpDataPo> findAll(Specification<RtpDataPo> spec, Pageable pageRequest);

	List<RtpDataPo> findAll(Specification<RtpDataPo> spec);
}
