package com.aemobile.casino.management.dao;

import org.springframework.data.repository.CrudRepository;

import com.aemobile.casino.management.domain.AccountPo;

public interface AuthDao extends CrudRepository<AccountPo, String> {

	AccountPo findByUsernameAndPassword(String username, String password);
}
