package com.aemobile.casino.management.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.aemobile.casino.management.domain.MachineDataPo;

public interface MachineDataDao extends PagingAndSortingRepository<MachineDataPo, Long> {

	Page<MachineDataPo> findAll(Specification<MachineDataPo> spec, Pageable pageRequest);
}
