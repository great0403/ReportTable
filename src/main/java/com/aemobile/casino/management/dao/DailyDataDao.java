package com.aemobile.casino.management.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.aemobile.casino.management.domain.DailyDataPo;

public interface DailyDataDao extends PagingAndSortingRepository<DailyDataPo, Long> {

	Page<DailyDataPo> findAll(Specification<DailyDataPo> spec, Pageable pageRequest);

	List<DailyDataPo> findAll(Specification<DailyDataPo> spec);
}
