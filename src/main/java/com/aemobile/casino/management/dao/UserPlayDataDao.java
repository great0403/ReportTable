package com.aemobile.casino.management.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.aemobile.casino.management.domain.UserPlayDataPo;

public interface UserPlayDataDao extends PagingAndSortingRepository<UserPlayDataPo, Long> {

	Page<UserPlayDataPo> findAll(Specification<UserPlayDataPo> spec, Pageable pageRequest);
}
