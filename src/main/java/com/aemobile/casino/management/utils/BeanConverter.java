/**
 * 
 */
package com.aemobile.casino.management.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cglib.core.Converter;

/**
 * @author Richard
 * 
 */
public class BeanConverter implements Converter {

	@SuppressWarnings("rawtypes")
	private final Class targetClass;

	@SuppressWarnings("rawtypes")
	public BeanConverter(Class targetClass) {
		this.targetClass = targetClass;
	}

	@SuppressWarnings("rawtypes")

	// value 源对象属性，target 目标对象属性类，context 目标对象setter方法名
	public Object convert(Object value, Class target, Object context) {
		if (value == null) {
			return null;
		}
		if (!target.isPrimitive() && target.equals(String.class) && value.getClass().equals(String.class)
				&& StringUtils.isNotBlank(value.toString())) {
			String fieldName = context.toString().replace("set", "");
			fieldName = fieldName.replace(fieldName.substring(0, 1), fieldName.substring(0, 1).toLowerCase());
		}

		return value;
	}
}
