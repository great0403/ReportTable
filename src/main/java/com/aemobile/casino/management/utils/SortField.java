/**
 * 
 */
package com.aemobile.casino.management.utils;

import java.io.Serializable;

import com.aemobile.casino.management.common.EDirection;

public class SortField implements Serializable {

	private static final long serialVersionUID = 1L;

	private EDirection direction;

	private String field;

	public SortField() {
	}

	public SortField(EDirection direction, String field) {
		this.direction = direction;
		this.field = field;
	}

	public EDirection getDirection() {
		return direction;
	}

	public void setDirection(EDirection direction) {
		this.direction = direction;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
}
