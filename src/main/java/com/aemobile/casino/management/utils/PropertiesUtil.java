/**
 * 
 */
package com.aemobile.casino.management.utils;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.aemobile.casino.management.common.EImageType;
import com.aemobile.casino.management.monitor.AELogger;

/**
 * @author Richard
 * 
 */
public class PropertiesUtil {

	private static Properties props;

	public static String getUrl(String icon, EImageType uri) {
		String value = "";
		if (StringUtils.isBlank(uri.getKey())) {
			value = props.getProperty("image.url.static");
		} else {
			value = props.getProperty(uri.getKey());
		}

		return value + "/" + icon;
	}

	public static void reloadProps() {
		try {
			Resource resource = new ClassPathResource("config.properties");
			props = PropertiesLoaderUtils.loadProperties(resource);
		} catch (IOException e) {
			AELogger.error(PropertiesUtil.class, "Reload props fail!");
		}
	}

	public static String getValue(String key) {
		return props.getProperty(key);
	}

	public static void setProps(Properties props) {
		PropertiesUtil.props = props;
	}

	public static String getFullIconPath(String icon) {
		if (StringUtils.isBlank(icon)) {
			return "";
		}
		if (icon.startsWith("https://") || icon.startsWith("http://")) {
			return icon;
		}

		return PropertiesUtil.getUrl(icon, EImageType.STATIC_DATA);
	}

}
