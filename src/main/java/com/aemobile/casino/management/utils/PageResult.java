package com.aemobile.casino.management.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PageResult<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int pageNum = 1;
	
	private int pageSize = 15;

	private long total;
	
	private List<T> dtoList = new ArrayList<T>();
	
	private List<SortField> sortList = new ArrayList<SortField>();

	public PageResult() {
	}

	public PageResult(int pageNum, SortField... sortList) {
		this.pageNum = pageNum;
		this.sortList.addAll(Arrays.asList(sortList));
	}

	public PageResult(int pageNum, int pageSize, long total, List<T> dtoList) {
		this.pageNum = pageNum;
		this.pageSize = pageSize;
		this.total = total;
		this.dtoList = dtoList;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public List<T> getDtoList() {
		return dtoList;
	}

	public void setDtoList(List<T> dtoList) {
		this.dtoList = dtoList;
	}

	public List<SortField> getSortList() {
		return sortList;
	}

	public void setSortList(List<SortField> sortList) {
		this.sortList = sortList;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public long getTotal() {
		return total;
	}


	public void setTotal(long total) {
		this.total = total;
	}

}
