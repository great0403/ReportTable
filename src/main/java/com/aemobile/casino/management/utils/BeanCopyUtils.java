package com.aemobile.casino.management.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.cglib.beans.BeanCopier;

import com.aemobile.casino.management.monitor.AELogger;

public class BeanCopyUtils {

	private static final ConcurrentHashMap<String, BeanCopier> copierMap = new ConcurrentHashMap<String, BeanCopier>();

	public static void copyProperties(final Object dest, final Object src) {
		BeanCopier copier = getBeanCopier(dest, src);
		copier.copy(src, dest, new BeanConverter(dest.getClass()));
	}

	public static <T> T copyProperties(final Object src, Class<T> clazz) {
		if (src != null && clazz != null) {
			try {
				T t = clazz.newInstance();
				BeanCopier copier = getBeanCopier(t, src);
				copier.copy(src, t, new BeanConverter(clazz));
				return t;
			} catch (Exception e) {
				AELogger.error(BeanCopyUtils.class, e.getMessage());
			}
		}
		return null;
	}

	public static BeanCopier getBeanCopier(final Object dest, final Object src) {
		String key = dest.getClass() + "##" + src.getClass();
		BeanCopier copier = copierMap.get(key);
		if (copier == null) {
			copier = BeanCopier.create(src.getClass(), dest.getClass(), true);
			copierMap.put(key, copier);
		}

		return copier;
	}

	public static <T, F> List<T> copyList(final Iterable<F> source, Class<T> clazz) {
		List<T> list = new ArrayList<T>();
		if (source != null) {
			for (F f : source) {
				try {
					T t = clazz.newInstance();
					copyProperties(t, f);
					list.add(t);
				} catch (Exception e) {
					AELogger.error(BeanCopyUtils.class, e.getMessage());
				}
			}
		}
		return list;
	}
}
