package com.aemobile.casino.management.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串处理工具类
 * 
 * @author Edison Shuai
 * 
 */
public class StringUtil {
	public static final String  DEFAULT_SERARATOR="-";
	
	/**
	 * 字符串转换为数组
	 * 
	 * @param str
	 * @param separator
	 *            分隔符，默认为“-”
	 * @return
	 */
	public static List<String> changeToList(String str, String separator){
		if("".equals(separator))
			separator = DEFAULT_SERARATOR;
		
		String[] array = str.split(separator);
		List<String> list = new ArrayList<String>();
		for(String s : array){
			if("".equals(s) == false)
				list.add(s);
		}
		return list; 
	}
	
	public static String[] split(String str, String separator) {
		if ("".equals(separator)) {
			separator = DEFAULT_SERARATOR;
		}
		return str.split(separator);
	}

	/**
	 * 将以"-"为分隔符的字符串转换为数组
	 * 
	 * @param str
	 * @return
	 */
	public static List<String> changeToList(String str){
		return changeToList(str, DEFAULT_SERARATOR);
	}
	public static String convertNull(String str){
		if(str==null)
			return "";
		return str;
	}
	
	/**
	 * 判断是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isBlank(String str){
		if(null == str || "".equals(str.trim())) return true;
		return false;		
	}
	
	
	public static String[] changeToArray(List<String> list){
		if(null == list)  return null;
		String[] result =  new String[list.size()];
		int index = 0;
		for(String str:list){
			result[index] = str;
			index++;
		}
		return result;
	}
	
	public static String getSuccessMsg(String msg) {
		return "<span style='color:green;font-size:14px'>" + msg + "</span>";
	}

	public static String getErrorMsg(String msg) {
		return "<span style='color:red;font-size:14px'>" + msg + "</span>";
	}

	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Pattern p = Pattern.compile("\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;
	}

}
