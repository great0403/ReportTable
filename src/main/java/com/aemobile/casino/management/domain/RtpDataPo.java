package com.aemobile.casino.management.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

//持久化类

@Entity
@Table(name = "rtp_data")
@DynamicInsert
@DynamicUpdate
public class RtpDataPo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "machine")
	private String machine;

	@Column(name = "date")
	private String date;

	@Column(name = "spin_count")
	private Long spinCount;

	@Column(name = "win_count")
	private Integer winCount;

	@Column(name = "win_probability")
	private Double winProbability;

	@Column(name = "free_spin_count")
	private Integer freeSpinCount;

	@Column(name = "free_spin_probability")
	private Double freeSpinProbability;

	@Column(name = "total_bet")
	private Long totalBet;

	@Column(name = "win_sum")
	private Long winSum;

	@Column(name = "quotient")
	private Double quotient;

	@Column(name = "rtp")
	private Double rtp;

	@Column(name = "normal_rtp")
	private Double normalRtp;

	@Column(name = "free_spin_rtp")
	private Double freeSpinRtp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getSpinCount() {
		return spinCount;
	}

	public void setSpinCount(Long spinCount) {
		this.spinCount = spinCount;
	}

	public Integer getWinCount() {
		return winCount;
	}

	public void setWinCount(Integer winCount) {
		this.winCount = winCount;
	}

	public Double getWinProbability() {
		return winProbability;
	}

	public void setWinProbability(Double winProbability) {
		this.winProbability = winProbability;
	}

	public Integer getFreeSpinCount() {
		return freeSpinCount;
	}

	public void setFreeSpinCount(Integer freeSpinCount) {
		this.freeSpinCount = freeSpinCount;
	}

	public Double getFreeSpinProbability() {
		return freeSpinProbability;
	}

	public void setFreeSpinProbability(Double freeSpinProbability) {
		this.freeSpinProbability = freeSpinProbability;
	}

	public Long getTotalBet() {
		return totalBet;
	}

	public void setTotalBet(Long totalBet) {
		this.totalBet = totalBet;
	}

	public Long getWinSum() {
		return winSum;
	}

	public void setWinSum(Long winSum) {
		this.winSum = winSum;
	}

	public Double getQuotient() {
		return quotient;
	}

	public void setQuotient(Double quotient) {
		this.quotient = quotient;
	}

	public Double getRtp() {
		return rtp;
	}

	public void setRtp(Double rtp) {
		this.rtp = rtp;
	}

	public Double getNormalRtp() {
		return normalRtp;
	}

	public void setNormalRtp(Double normalRtp) {
		this.normalRtp = normalRtp;
	}

	public Double getFreeSpinRtp() {
		return freeSpinRtp;
	}

	public void setFreeSpinRtp(Double freeSpinRtp) {
		this.freeSpinRtp = freeSpinRtp;
	}

}
