package com.aemobile.casino.management.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "daily_data")
@DynamicInsert
@DynamicUpdate
public class DailyDataPo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "date")
	private String date;

	@Column(name = "country")
	private String country;

	@Column(name = "version")
	private String version;

	@Column(name = "new_user_count")
	private Integer newUserCount;

	@Column(name = "active_user_count")
	private Integer activeUserCount;

	@Column(name = "average_login_count")
	private Integer averageLoginCount;

	@Column(name = "average_stay_minutes")
	private Integer averageStayMinutes;

	@Column(name = "pay_user_count")
	private Integer payUserCount;

	@Column(name = "new_user_pay_count")
	private Integer newUserPayCount;

	@Column(name = "new_pay_user_count")
	private Integer newPayUserCount;

	@Column(name = "order_count")
	private Integer orderCount;

	@Column(name = "pay_sum")
	private Double paySum;

	@Column(name = "pay_rate")
	private Double payRate;

	@Column(name = "arppdau")
	private Double arppdau;

	@Column(name = "arpdau")
	private Double arpdau;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getNewUserCount() {
		return newUserCount;
	}

	public void setNewUserCount(Integer newUserCount) {
		this.newUserCount = newUserCount;
	}

	public Integer getActiveUserCount() {
		return activeUserCount;
	}

	public void setActiveUserCount(Integer activeUserCount) {
		this.activeUserCount = activeUserCount;
	}

	public Integer getAverageLoginCount() {
		return averageLoginCount;
	}

	public void setAverageLoginCount(Integer averageLoginCount) {
		this.averageLoginCount = averageLoginCount;
	}

	public Integer getAverageStayMinutes() {
		return averageStayMinutes;
	}

	public void setAverageStayMinutes(Integer averageStayMinutes) {
		this.averageStayMinutes = averageStayMinutes;
	}

	public Integer getPayUserCount() {
		return payUserCount;
	}

	public void setPayUserCount(Integer payUserCount) {
		this.payUserCount = payUserCount;
	}

	public Integer getNewUserPayCount() {
		return newUserPayCount;
	}

	public void setNewUserPayCount(Integer newUserPayCount) {
		this.newUserPayCount = newUserPayCount;
	}

	public Integer getNewPayUserCount() {
		return newPayUserCount;
	}

	public void setNewPayUserCount(Integer newPayUserCount) {
		this.newPayUserCount = newPayUserCount;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public Double getPaySum() {
		return paySum;
	}

	public void setPaySum(Double paySum) {
		this.paySum = paySum;
	}

	public Double getPayRate() {
		return payRate;
	}

	public void setPayRate(Double payRate) {
		this.payRate = payRate;
	}

	public Double getArppdau() {
		return arppdau;
	}

	public void setArppdau(Double arppdau) {
		this.arppdau = arppdau;
	}

	public Double getArpdau() {
		return arpdau;
	}

	public void setArpdau(Double arpdau) {
		this.arpdau = arpdau;
	}

}
