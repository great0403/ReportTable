package com.aemobile.casino.management.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "user_play_data") // @Table注解用来标识实体类与数据表的对应关系
@DynamicInsert
@DynamicUpdate
public class UserPlayDataPo {

	@Id // 用于声明一个实体类的属性映射为数据库的主键列
	@GeneratedValue(strategy = GenerationType.IDENTITY) // 用于标注主键的生成策略，通过strategy属性指定
	@Column(name = "id")
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "date")
	private String date;

	@Column(name = "login_count")
	private Integer loginCount;

	@Column(name = "stay_minutes")
	private Integer stayMinutes;

	@Column(name = "machine_map")
	private String machineMap;

	@Column(name = "bet_map")
	private String betMap;

	@Column(name = "country")
	private String country;

	@Column(name = "version")
	private String version;

	@Column(name = "is_new")
	private Boolean idNew;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public Integer getStayMinutes() {
		return stayMinutes;
	}

	public void setStayMinutes(Integer stayMinutes) {
		this.stayMinutes = stayMinutes;
	}

	public String getMachineMap() {
		return machineMap;
	}

	public void setMachineMap(String machineMap) {
		this.machineMap = machineMap;
	}

	public String getBetMap() {
		return betMap;
	}

	public void setBetMap(String betMap) {
		this.betMap = betMap;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getIdNew() {
		return idNew;
	}

	public void setIdNew(Boolean idNew) {
		this.idNew = idNew;
	}

}
