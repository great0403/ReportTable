package com.aemobile.casino.management.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "machine_data") // @Table注解用来标识实体类与数据表的对应关系
@DynamicInsert
@DynamicUpdate
public class MachineDataPo {

	@Id // 用于声明一个实体类的属性映射为数据库的主键列
	@GeneratedValue(strategy = GenerationType.IDENTITY) // 用于标注主键的生成策略，通过strategy属性指定
	@Column(name = "id")
	private Long id;

	@Column(name = "date")
	private String date;

	@Column(name = "machine")
	private String machine;

	@Column(name = "user_count")
	private Integer userCount;

	@Column(name = "spin_count")
	private Long spinCount;

	@Column(name = "average_count")
	private Integer averageCount;

	@Column(name = "win_probability")
	private Double winProbability;

	@Column(name = "rtp")
	private Double rtp;

	@Column(name = "free_spin_probability")
	private Double freeSpinProbability;

	@Column(name = "bet_map")
	private String betMap;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	public Long getSpinCount() {
		return spinCount;
	}

	public void setSpinCount(Long spinCount) {
		this.spinCount = spinCount;
	}

	public Integer getAverageCount() {
		return averageCount;
	}

	public void setAverageCount(Integer averageCount) {
		this.averageCount = averageCount;
	}

	public Double getWinProbability() {
		return winProbability;
	}

	public void setWinProbability(Double winProbability) {
		this.winProbability = winProbability;
	}

	public Double getRtp() {
		return rtp;
	}

	public void setRtp(Double rtp) {
		this.rtp = rtp;
	}

	public Double getFreeSpinProbability() {
		return freeSpinProbability;
	}

	public void setFreeSpinProbability(Double freeSpinProbability) {
		this.freeSpinProbability = freeSpinProbability;
	}

	public String getBetMap() {
		return betMap;
	}

	public void setBetMap(String betMap) {
		this.betMap = betMap;
	}

}
