package com.aemobile.casino.management.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aemobile.casino.management.common.Management;
import com.aemobile.casino.management.dto.DailyData;
import com.aemobile.casino.management.service.DailyDataService;
import com.aemobile.casino.management.utils.PageResult;

@Controller
@RequestMapping(value = "/dailyData")
public class DailyDataController {

	@Autowired
	private DailyDataService service;
	
	@RequestMapping(method = RequestMethod.GET)
	private String index(final Model model, final HttpServletRequest request,
			@RequestParam(value = "id", defaultValue = "") Integer id,
			@RequestParam(value = "country", defaultValue = "") String country,
			@RequestParam(value = "version", defaultValue = "") String version,
			@RequestParam(value = "date", defaultValue = "") String date) {
		PageResult<DailyData> result = service.getDailyDataByUserId(country, version, date, 1, 1);

		// List<String> versionList = service.getVersionList(id, country, date);

		model.addAttribute("pageNum", 1);
		model.addAttribute("country", country);
		model.addAttribute("version", version);
		// model.addAttribute("versionList",versionList);
		model.addAttribute("date", date);
		model.addAttribute("pageSize", Management.DEFAULT_PAGE_SIZE_STR);
		model.addAttribute("total", result.getTotal());
		return "/dailyData/index";
	}

	@RequestMapping(value = "/table", method = RequestMethod.GET)
	public String viewList(final Model model, final HttpServletRequest request,
			@RequestParam(value = "page", defaultValue = Management.DEFAULT_PAGE_STR) String page,
			@RequestParam(value = "size", defaultValue = Management.DEFAULT_PAGE_SIZE_STR) String size,
			@RequestParam(value = "country", defaultValue = "") String country,
			@RequestParam(value = "version", defaultValue = "") String version,
			@RequestParam(value = "date", defaultValue = "") String date) {
		int pageNum = Integer.parseInt(page);
		int pageSize = Integer.parseInt(size);
		PageResult<DailyData> result = service.getDailyDataByUserId(country, version, date, pageNum, pageSize);

		model.addAttribute("pageNum", pageNum);
		model.addAttribute("country", country);
		model.addAttribute("version", version);
		model.addAttribute("date", date);
		model.addAttribute("total", result.getTotal());
		model.addAttribute("result", result.getDtoList());
		return "/dailyData/indexTable";
	}

}
