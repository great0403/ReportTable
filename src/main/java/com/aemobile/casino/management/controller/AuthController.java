package com.aemobile.casino.management.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aemobile.casino.management.common.ErrorMessage;
import com.aemobile.casino.management.common.Permission;
import com.aemobile.casino.management.dto.MenuItem;

@Controller
public class AuthController {

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String doLogin(final Model model, @RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password) {
		System.out.println("username:" + username + ",passsword:" + password);
		Subject subject = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(username, password);

		// final Subject currentUser = new WebSubject.Builder(servletRequest,
		// servletResponse).buildSubject();
		// currentUser.login(token);
		// ThreadContext.bind(currentUser);

		try {
			if (subject.isAuthenticated()) {
				// 踢掉上一个用户
				subject.logout();
			}
			// 登录
			subject.login(token);
			subject.getSession().setAttribute("user", username);
			subject.getSession().setAttribute("menus", getMenu(subject));
		} catch (AuthenticationException e) {
			// 身份验证失败
			model.addAttribute("errorMsg", ErrorMessage.LOGIN_FAIL);
			System.out.println("身份验证失败——username22222:" + username + ",passsword:" + password);
			return "/login";
		}
		return "redirect:/home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPage(final Model model) {
		return "/login";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String userLogout(final Model model) {
		Subject subject = SecurityUtils.getSubject();
		if (subject.isAuthenticated()) {
			subject.logout();
		}
		return "/login";
	}

	private List<MenuItem> getMenu(Subject subject) {
		MenuItem operationMenu = new MenuItem("运营配置", "", true);

		if (subject.isPermitted(Permission.VIEW_ANNOUNCEMENT) || subject.isPermitted(Permission.EDIT_ANNOUNCEMENT)) {
			operationMenu.getChildren().add(new MenuItem("MachineData", "machineData", false));
			operationMenu.getChildren().add(new MenuItem("RtpData", "rtpData", false));
			operationMenu.getChildren().add(new MenuItem("UserPlayData", "userPlayData", false));
			operationMenu.getChildren().add(new MenuItem("DailyData", "dailyData", false));
		}
		return Arrays.asList(operationMenu);
	}
}
