package com.aemobile.casino.management.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aemobile.casino.management.common.Management;
import com.aemobile.casino.management.dto.UserPlayData;
import com.aemobile.casino.management.service.UserPlayDataService;
import com.aemobile.casino.management.utils.PageResult;

@Controller
@RequestMapping(value = "/userPlayData")
public class UserPlayDataController {

	@Autowired
	private UserPlayDataService service;

	@RequestMapping(method = RequestMethod.GET)

	private String index(final Model model, final HttpServletRequest request,
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "userId", defaultValue = "") Long userId) {
		PageResult<UserPlayData> result = service.getUserPlayDataByUserId(id,
				userId, 1, 1);
		
		model.addAttribute("pageNum",1);
		model.addAttribute("id", id);
		model.addAttribute("userId",userId);
		model.addAttribute("pageSize",Management.DEFAULT_PAGE_SIZE_STR);
		model.addAttribute("total",result.getTotal());
		return "/userPlayData/index";
	}

	@RequestMapping(value = "/table", method = RequestMethod.GET)
	public String viewList(final Model model, final HttpServletRequest request,
			@RequestParam(value = "page", defaultValue = Management.DEFAULT_PAGE_STR) String page,
			@RequestParam(value = "size", defaultValue = Management.DEFAULT_PAGE_SIZE_STR) String size,
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "userId", defaultValue = "") Long userId,
			@RequestParam(value = "date", defaultValue = "") Long date) {
		int pageNum = Integer.parseInt(page);
		int pageSize = Integer.parseInt(size);
		PageResult<UserPlayData> result = service.getUserPlayDataByUserId(id, userId, pageNum, pageSize);
		
		model.addAttribute("pageNum", pageNum);
		model.addAttribute("id", id);
		model.addAttribute("userId", userId);
		model.addAttribute("total", result.getTotal());
		model.addAttribute("result", result.getDtoList());
		return "/userPlayData/indexTable";
	}
}
