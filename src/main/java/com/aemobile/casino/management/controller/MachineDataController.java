package com.aemobile.casino.management.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.aemobile.casino.management.common.Management;
import com.aemobile.casino.management.dto.MachineData;
import com.aemobile.casino.management.gameconfig.EMachine;
import com.aemobile.casino.management.service.MachineDataService;
import com.aemobile.casino.management.utils.PageResult;

@Controller
@RequestMapping(value = "/machineData")
public class MachineDataController{

	@Autowired
	private MachineDataService service;

	@RequestMapping(method = RequestMethod.GET)
	private String index(final Model model, final HttpServletRequest request,
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "machine", defaultValue = "") String machine,
			@RequestParam(value = "date", defaultValue = "") String date) {
		
		// EMachine machineEnum = EMachine.getByCode(machine);
		PageResult<MachineData> result = service.getMachineDataByUserId(id,
				machine, date, 1, 1);
		
		model.addAttribute("pageNum",1);
		model.addAttribute("id", id);
		model.addAttribute("machine", machine);
		model.addAttribute("machineList",EMachine.getMachineList(machine));
		model.addAttribute("date",date);
		model.addAttribute("pageSize",Management.DEFAULT_PAGE_SIZE_STR);
		model.addAttribute("total",result.getTotal());
		return "/machineData/index";
	}

	@RequestMapping(value = "/table", method = RequestMethod.GET)
	public String viewList(final Model model, final HttpServletRequest request,
			@RequestParam(value = "page", defaultValue = Management.DEFAULT_PAGE_STR) String page,
			@RequestParam(value = "size", defaultValue = Management.DEFAULT_PAGE_SIZE_STR) String size,
			@RequestParam(value = "id", defaultValue = "") String id,
			@RequestParam(value = "machine", defaultValue = "") String machine,
			@RequestParam(value = "date", defaultValue = "") String date) {
		int pageNum = Integer.parseInt(page);
		int pageSize = Integer.parseInt(size);
		PageResult<MachineData> result = service.getMachineDataByUserId(id, machine, date, pageNum, pageSize);
		
		model.addAttribute("pageNum", pageNum);
		model.addAttribute("id", id);
		model.addAttribute("machine", machine);
		model.addAttribute("date", date);
		model.addAttribute("total", result.getTotal());
		model.addAttribute("result", result.getDtoList());
		return "/machineData/indexTable";
	}

	// private String getDate(final Model model, final HttpServletRequest
	// request,
	// @RequestParam(value = "date", defaultValue = "") String date) throws
	// ParseException {
	//
	// Calendar calendar = Calendar.getInstance();
	// String startDate = "";
	// String endDate = "";
	// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	// Date dBegin = sdf.parse(startDate);
	// Date dEnd = sdf.parse(endDate);
	//
	// if (dBegin.getTime() > dEnd.getTime()) {
	// System.out.println("日期选择错误");
	// }

//		List<MachineData> lDate = findByStartDateBetween(startDate, endDate);
//		return null;
	}

//	@Override
//	public List<MachineData> findByStartDateBetween(String startDate, String endDate) {
		
		// System.out.println(list<MachineData> machineDataInfo);
		// lDate.add(dBegin);
		// Calendar calBegin = Calendar.getInstance();
		// calBegin.setTime(dBegin);
		//
		// Calendar calEnd = Calendar.getInstance();
		// calEnd.setTime(dEnd);
		//
		// while (dEnd.after(calBegin.getTime())) {
		// calBegin.add(Calendar.DAY_OF_YEAR, -7);
		// lDate.add(calBegin.getTime());
		// }
		// PageResult<MachineData> result = service.getMachineDataByUserId(id,
		// machine, date, pageNum, pageSize);
		// return List<MachineData> machineDataInfo;
// }
