package com.aemobile.casino.management.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String mainPage(final Model model, final HttpServletRequest request) {
		return "redirect:/machineData";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String enterHome(final Model model, final HttpServletRequest request) {

		return "redirect:/machineData";
	}

}
