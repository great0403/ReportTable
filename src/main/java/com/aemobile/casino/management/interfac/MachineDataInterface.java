package com.aemobile.casino.management.interfac;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.aemobile.casino.management.dto.MachineData;

public interface MachineDataInterface {

	@Query("SELECT * FROM MachineDataPo WHERE date BETWEEN 'startDate' and 'endDate'")
	List<MachineData> findByStartDateBetween(@Param("startDate") String startDate, @Param("endDate") String endDate);

}
