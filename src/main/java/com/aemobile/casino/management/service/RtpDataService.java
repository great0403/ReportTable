package com.aemobile.casino.management.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aemobile.casino.management.dao.RtpDataDao;
import com.aemobile.casino.management.domain.RtpDataPo;
import com.aemobile.casino.management.dto.RtpData;
import com.aemobile.casino.management.utils.BeanCopyUtils;
import com.aemobile.casino.management.utils.PageResult;

@Service
@Transactional
public class RtpDataService {

	@Autowired
	private RtpDataDao dao;

	public PageResult<RtpData> getRtpDataList(int page, int pageSize) {
		Page<RtpDataPo> poList = dao
				.findAll(new PageRequest(page - 1, pageSize, Direction.ASC, new String[] { "id" }));
		List<RtpData> list = BeanCopyUtils.copyList(poList, RtpData.class);

		return new PageResult<RtpData>(page, pageSize, poList.getTotalElements(), list);
	}

	public PageResult<RtpData> getRtpDataByUserId(final String id, final String machine, final String date, int pageNum,
			int pageSize) {
		Specification<RtpDataPo> spec = new Specification<RtpDataPo>() {
			public Predicate toPredicate(Root<RtpDataPo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> idField = root.get("id");
				Path<String> machineField = root.get("machine");
				Path<String> dateField = root.get("date");

				List<Predicate> predicateList = new ArrayList<Predicate>();
				if (!id.trim().equals("")) {
					predicateList.add(cb.equal(idField, id));
				}
				if (!machine.trim().equals("")) {
					predicateList.add(cb.equal(machineField, machine));
				}
				// if (!date.trim().equals("")) {
				// predicateList.add(cb.equal(dateField, date));
				// }
				if (!predicateList.isEmpty()) {
					query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				}
				return null;
			}
		};
		Page<RtpDataPo> poList = dao.findAll(spec, new PageRequest(pageNum - 1, pageSize));

		List<RtpData> dtoList = new ArrayList<RtpData>();
		for (RtpDataPo po : poList) {
			RtpData dto = BeanCopyUtils.copyProperties(po, RtpData.class);
			dtoList.add(dto);
		}
		PageResult<RtpData> result = new PageResult<RtpData>(pageNum, pageSize, poList.getTotalElements(), dtoList);
		return result;
	}

}
