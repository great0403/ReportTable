package com.aemobile.casino.management.service;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.aemobile.casino.management.dao.UserPlayDataDao;
import com.aemobile.casino.management.domain.UserPlayDataPo;
import com.aemobile.casino.management.dto.UserPlayData;
import com.aemobile.casino.management.utils.BeanCopyUtils;
import com.aemobile.casino.management.utils.PageResult;

@Service
@Transactional
public class UserPlayDataService {

	@Autowired
	private UserPlayDataDao dao;

	public PageResult<UserPlayData> getUserPlayDataList(int page, int pageSize) {
		Page<UserPlayDataPo> poList = dao
				.findAll(new PageRequest(page - 1, pageSize, Direction.ASC, new String[] { "id" }));
		
		List<UserPlayData> list = BeanCopyUtils.copyList(poList, UserPlayData.class);
		return new PageResult<UserPlayData>(page, pageSize, poList.getTotalElements(), list);
	}

	public PageResult<UserPlayData> getUserPlayDataByUserId(final String id, final Long userId, int pageNum,
			int pageSize) {
		Specification<UserPlayDataPo> spec = new Specification<UserPlayDataPo>() {
			public Predicate toPredicate(Root<UserPlayDataPo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> idField = root.get("id");
				Path<Long> userIdField = root.get("userId");

				List<Predicate> predicateList = new ArrayList<Predicate>();
				if (!id.trim().equals("")) {
					predicateList.add(cb.equal(idField, id));
				}
				// if (!userId.equals("")) {
				// predicateList.add(cb.equal(userIdField, userId));
				// }
				// if (!date.equals("")) {
				// predicateList.add(cb.equal(dateField, date));
				// }
				if (!predicateList.isEmpty()) {
					query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				}
				return null;
			}
		};

		Page<UserPlayDataPo> poList = dao.findAll(spec, new PageRequest(pageNum - 1, pageSize));

		List<UserPlayData> dtoList = new ArrayList<UserPlayData>();
		for (UserPlayDataPo po : poList) {
			UserPlayData dto = BeanCopyUtils.copyProperties(po, UserPlayData.class);
			dtoList.add(dto);
		}
		PageResult<UserPlayData> result = new PageResult<UserPlayData>(pageNum, pageSize, poList.getTotalElements(),
				dtoList);
		return result;
	}
}
