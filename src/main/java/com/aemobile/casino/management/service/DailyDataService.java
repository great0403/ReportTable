package com.aemobile.casino.management.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.aemobile.casino.management.dao.DailyDataDao;
import com.aemobile.casino.management.domain.DailyDataPo;
import com.aemobile.casino.management.dto.DailyData;
import com.aemobile.casino.management.utils.BeanCopyUtils;
import com.aemobile.casino.management.utils.PageResult;

@Service
@Transactional
public class DailyDataService {

	@Autowired
	private DailyDataDao dao;

	public PageResult<DailyData> getDailyDataList(int page, int pageSize) {
		Page<DailyDataPo> poList = dao
				.findAll(new PageRequest(page - 1, pageSize, Direction.ASC, new String[] { "id" }));
		List<DailyData> list = BeanCopyUtils.copyList(poList, DailyData.class);

		return new PageResult<DailyData>(page, pageSize, poList.getTotalElements(), list);
	}

	public PageResult<DailyData> getDailyDataByUserId(final String country, final String version, final String date,
			int pageNum, int pageSize) {
		Specification<DailyDataPo> spec = new Specification<DailyDataPo>() {
			public Predicate toPredicate(Root<DailyDataPo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> idField = root.get("id");
				Path<String> versionField = root.get("version");
				Path<String> dateField = root.get("date");

				System.out.println("idField:" + idField + " versionField" + versionField + " dateField:" + dateField);
				List<Predicate> predicateList = new ArrayList<Predicate>();
				// if (!id.equals("")) {
				// predicateList.add(cb.equal(countryField, country));
				// }
				if (!version.trim().equals("")) {
					predicateList.add(cb.equal(versionField, version));
				}
				// if (!date.trim().equals("")) {
				// predicateList.add(cb.equal(dateField, date));
				// }
				if (!predicateList.isEmpty()) {
					query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				}
				return null;
			}
		};

		Page<DailyDataPo> poList = dao.findAll(spec, new PageRequest(pageNum - 1, pageSize));

		List<DailyData> dtoList = new ArrayList<DailyData>();
		for (DailyDataPo po : poList) {
			DailyData dto = BeanCopyUtils.copyProperties(po, DailyData.class);
			dtoList.add(dto);
		}
		PageResult<DailyData> result = new PageResult<DailyData>(pageNum, pageSize, poList.getTotalElements(), dtoList);
		return result;
	}

	public List<String> getVersionList(final int id, final String country, final String date) {
		Specification<DailyDataPo> spec = new Specification<DailyDataPo>() {

			public Predicate toPredicate(Root<DailyDataPo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<Integer> idField = root.get("id");
				Path<Integer> countryField = root.get("country");
				Path<Integer> dateField = root.get("date");

				List<Predicate> predicateList = new ArrayList<Predicate>();
				if (id > 0) {
					predicateList.add(cb.equal(idField, id));
				}
				if (StringUtils.isNotBlank(country)) {
					predicateList.add(cb.equal(countryField, country));
				}

				if (StringUtils.isNotBlank(date)) {
					predicateList.add(cb.equal(dateField, date));
				}

				if (!predicateList.isEmpty()) {
					query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				}
				return null;
			}
		};

		List<DailyDataPo> poList = dao.findAll(spec);
		Set<String> versionSet = new HashSet<String>();
		List<String> versionList = new ArrayList<String>();
		if (CollectionUtils.isNotEmpty(poList)) {
			for (DailyDataPo po : poList) {
				versionSet.add(po.getVersion());
			}
		}
		if (!versionSet.isEmpty()) {
			versionList.addAll(versionSet);
			Collections.sort(versionList);
		}

		return versionList;
	}
}
