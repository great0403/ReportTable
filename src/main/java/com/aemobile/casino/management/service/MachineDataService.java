package com.aemobile.casino.management.service;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.aemobile.casino.management.dao.MachineDataDao;
import com.aemobile.casino.management.domain.MachineDataPo;
import com.aemobile.casino.management.dto.MachineData;
import com.aemobile.casino.management.utils.BeanCopyUtils;
import com.aemobile.casino.management.utils.PageResult;

@Service
@Transactional
public class MachineDataService {

	@Autowired
	private MachineDataDao dao;

	public PageResult<MachineData> getMachineDataList(int page,int pageSize) {
		Page<MachineDataPo> poList = dao
				.findAll(new PageRequest(page - 1, pageSize, Direction.ASC, new String[] { "id" }));
		
		List<MachineData> list = BeanCopyUtils.copyList(poList, MachineData.class);
		return new PageResult<MachineData>(page, pageSize, poList.getTotalElements(),list);
	}

	public PageResult<MachineData> getMachineDataByUserId(final String id, final String machine, final String date,
			int pageNum, int pageSize) {
		Specification<MachineDataPo> spec = new Specification<MachineDataPo>() {
			public Predicate toPredicate(Root<MachineDataPo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Path<String> idField = root.get("id");
				Path<String> machineField = root.get("machine");
				Path<String> dateField = root.get("date");

				List<Predicate> predicateList = new ArrayList<Predicate>();
				if (!id.trim().equals("")) {
					predicateList.add(cb.equal(idField, id));
				}
				if (!machine.trim().equals("")) {
					predicateList.add(cb.equal(machineField, machine));
				}
				// if (!date.trim().equals("")) {
				// predicateList.add(cb.equal(dateField, date));
				// }
				if (!predicateList.isEmpty()) {
					query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				}
				return null;
			}
		};

		Page<MachineDataPo> poList = dao.findAll(spec, new PageRequest(pageNum - 1, pageSize));

		List<MachineData> dtoList = new ArrayList<MachineData>();
		for (MachineDataPo po : poList) {
			MachineData dto = BeanCopyUtils.copyProperties(po, MachineData.class);
			dtoList.add(dto);
		}
		PageResult<MachineData> result = new PageResult<MachineData>(pageNum, pageSize, poList.getTotalElements(),
				dtoList);
		return result;
	}
}
