/**
 * 
 */
package com.aemobile.casino.management.component;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aemobile.casino.management.dao.AuthDao;
import com.aemobile.casino.management.domain.AccountPo;

/**
 * @author Richard
 * 
 */
@Component("casinoRealm")
public class CasinoRealm extends AuthorizingRealm {

	@Autowired
	private AuthDao authDao;

	/**
	 * 授权信息
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String username = (String) principals.fromRealm(getName()).iterator().next();
		if (username != null) {
			AccountPo user = authDao.findOne(username);
			if (user != null) {
				SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
				if (StringUtils.isNotBlank(user.getRole())) {
					info.addRoles(Arrays.asList(user.getRole().split(":")));
				}
				if (StringUtils.isNotBlank(user.getPermission())) {
					info.addStringPermissions(Arrays.asList(user.getPermission().split(",")));
				}
				return info;
			}
		}
		return null;
	}

	/**
	 * 认证信息
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		String username = token.getUsername();
		if (StringUtils.isNotBlank(username)) {
			AccountPo user = authDao.findByUsernameAndPassword(username, String.valueOf(token.getPassword()));

			if (user != null) {
				return new SimpleAuthenticationInfo(username, user.getPassword(), getName());
			}
		}
		return null;
	}

}
