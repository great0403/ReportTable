package com.aemobile.casino.management.gameconfig;

import java.util.ArrayList;
import java.util.List;

public enum EMachine {

	ALL(0, "default"),

	WOLF(1, null),

	GOD_OF_FORTUNE(2, null),

	ALADDIN(3, null),

	BUFFALO(4, null),

	ADVENTURE(5, null);

	private int code;
	private String machine;

	private EMachine(int code, String machine) {
		this.code = code;
		this.machine = machine;
	}

	public static EMachine getByCode(int code) {
		for (EMachine m : EMachine.values()) {
			if (m.getCode() == code) {
				return m;
			}
		}
		return ALL;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}
	
	public static List<EMachine> getMachineList(String machine) {
		List<EMachine> list = new ArrayList<EMachine>();
		if (machine != null) {
			list.add(ALL);
			for (EMachine source : EMachine.values()) {
				if (source.getMachine() == machine) {
					list.add(source);
				}
			}
		}
		return list;
	}

	public static EMachine getByCode(String string) {
		// TODO Auto-generated method stub
		return null;
	}
}
