package com.aemobile.casino.management.gameconfig;

public enum EMachineDate {

	NEARLY_7, NEARLY_30, THIS_MONTH, LAST_MONTH, CUSTOMIZE;
}
