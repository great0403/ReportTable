package com.aemobile.casino.management.gameconfig;

public enum EDayCountry {

	ALL(),

	AMERICA(),

	AUSTRALIA(),

	ENGLAND(),

	HONGKONG(),

	MACAO(),

	CHINA(),

	GERMANY(),

	FRANCE(),

	ITALY();

}
