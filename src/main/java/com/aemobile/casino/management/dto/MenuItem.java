/**
 * 
 */
package com.aemobile.casino.management.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Richard
 *
 */
public class MenuItem implements Serializable {

	private static final long serialVersionUID = 1L;

	private String title;
	private String link;
	private List<MenuItem> children;

	public MenuItem(String title, String link, boolean hasChild) {
		this.title = title;
		this.link = link;
		if (hasChild) {
			children = new ArrayList<MenuItem>();
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}


	public void setLink(String link) {
		this.link = link;
	}


	public List<MenuItem> getChildren() {
		return children;
	}

	public void setChildren(List<MenuItem> children) {
		this.children = children;
	}

}
