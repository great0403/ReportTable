package com.aemobile.casino.management.dto;

import java.io.Serializable;

public class MachineData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String date;
	private String machine;
	private Integer userCount;
	private Long spinCount;
	private Integer averageCount;
	private Double winProbability;
	private Double rtp;
	private Double freeSpinProbability;
	private String betMap;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	public Long getSpinCount() {
		return spinCount;
	}

	public void setSpinCount(Long spinCount) {
		this.spinCount = spinCount;
	}

	public Integer getAverageCount() {
		return averageCount;
	}

	public void setAverageCount(Integer averageCount) {
		this.averageCount = averageCount;
	}

	public Double getWinProbability() {
		return winProbability;
	}

	public void setWinProbability(Double winProbability) {
		this.winProbability = winProbability;
	}
	public Double getRtp() {
		return rtp;
	}
	public void setRtp(Double rtp) {
		this.rtp = rtp;
	}

	public Double getFreeSpinProbability() {
		return freeSpinProbability;
	}

	public void setFreeSpinProbability(Double freeSpinProbability) {
		this.freeSpinProbability = freeSpinProbability;
	}

	public String getBetMap() {
		return betMap;
	}

	public void setBetMap(String betMap) {
		this.betMap = betMap;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
