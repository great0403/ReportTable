package com.aemobile.casino.management.dto;

import java.io.Serializable;

public class RtpData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String machine;
	private String date;
	private Long spinCount;
	private Integer winCount;
	private Double winProbability;
	private Integer freeSpinCount;
	private Double freeSpinProbability;
	private Long totalBet;
	private Long winSum;
	private Double quotient;
	private Double rtp;
	private Double normalRtp;
	private Double freeSpinRtp;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMachine() {
		return machine;
	}
	public void setMachine(String machine) {
		this.machine = machine;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Long getSpinCount() {
		return spinCount;
	}
	public void setSpinCount(Long spinCount) {
		this.spinCount = spinCount;
	}
	public Integer getWinCount() {
		return winCount;
	}
	public void setWinCount(Integer winCount) {
		this.winCount = winCount;
	}

	public Double getWinProbability() {
		return winProbability;
	}

	public void setWinProbability(Double winProbability) {
		this.winProbability = winProbability;
	}

	public Integer getFreeSpinCount() {
		return freeSpinCount;
	}

	public void setFreeSpinCount(Integer freeSpinCount) {
		this.freeSpinCount = freeSpinCount;
	}

	public Double getFreeSpinProbability() {
		return freeSpinProbability;
	}

	public void setFreeSpinProbability(Double freeSpinProbability) {
		this.freeSpinProbability = freeSpinProbability;
	}
	public Long getTotalBet() {
		return totalBet;
	}
	public void setTotalBet(Long totalBet) {
		this.totalBet = totalBet;
	}
	public Long getWinSum() {
		return winSum;
	}
	public void setWinSum(Long winSum) {
		this.winSum = winSum;
	}
	public Double getQuotient() {
		return quotient;
	}
	public void setQuotient(Double quotient) {
		this.quotient = quotient;
	}
	public Double getRtp() {
		return rtp;
	}
	public void setRtp(Double rtp) {
		this.rtp = rtp;
	}
	public Double getNormalRtp() {
		return normalRtp;
	}
	public void setNormalRtp(Double normalRtp) {
		this.normalRtp = normalRtp;
	}
	public Double getFreeSpinRtp() {
		return freeSpinRtp;
	}
	public void setFreeSpinRtp(Double freeSpinRtp) {
		this.freeSpinRtp = freeSpinRtp;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}