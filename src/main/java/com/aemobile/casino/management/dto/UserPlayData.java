package com.aemobile.casino.management.dto;

import java.io.Serializable;

public class UserPlayData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private Long userId;
	private String date;
	private Integer loginCount;
	private Integer stayMinutes;
	private String machineMap;
	private String betMap;
	private String country;
	private String version;
	private Boolean idNew;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getLoginCount() {
		return loginCount;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public Integer getStayMinutes() {
		return stayMinutes;
	}

	public void setStayMinutes(Integer stayMinutes) {
		this.stayMinutes = stayMinutes;
	}

	public String getMachineMap() {
		return machineMap;
	}

	public void setMachineMap(String machineMap) {
		this.machineMap = machineMap;
	}

	public String getBetMap() {
		return betMap;
	}

	public void setBetMap(String betMap) {
		this.betMap = betMap;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Boolean getIdNew() {
		return idNew;
	}

	public void setIdNew(Boolean idNew) {
		this.idNew = idNew;
	}

}
