package com.aemobile.casino.management.dto;

import java.io.Serializable;

public class DailyData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String date;
	private String country;
	private String version;
	private Integer newUserCount;
	private Integer activeUserCount;
	private Integer averageLoginCount;
	private Integer averageStayMinutes;
	private Integer payUserCount;
	private Integer newUserPayCount;
	private Integer newPayUserCount;
	private Integer orderCount;
	private Double paySum;
	private Double payRate;
	private Double arppdau;
	private Double arpdau;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getNewUserCount() {
		return newUserCount;
	}

	public void setNewUserCount(Integer newUserCount) {
		this.newUserCount = newUserCount;
	}

	public Integer getActiveUserCount() {
		return activeUserCount;
	}

	public void setActiveUserCount(Integer activeUserCount) {
		this.activeUserCount = activeUserCount;
	}

	public Integer getAverageLoginCount() {
		return averageLoginCount;
	}

	public void setAverageLoginCount(Integer averageLoginCount) {
		this.averageLoginCount = averageLoginCount;
	}

	public Integer getAverageStayMinutes() {
		return averageStayMinutes;
	}

	public void setAverageStayMinutes(Integer averageStayMinutes) {
		this.averageStayMinutes = averageStayMinutes;
	}

	public Integer getPayUserCount() {
		return payUserCount;
	}

	public void setPayUserCount(Integer payUserCount) {
		this.payUserCount = payUserCount;
	}

	public Integer getNewUserPayCount() {
		return newUserPayCount;
	}

	public void setNewUserPayCount(Integer newUserPayCount) {
		this.newUserPayCount = newUserPayCount;
	}

	public Integer getNewPayUserCount() {
		return newPayUserCount;
	}

	public void setNewPayUserCount(Integer newPayUserCount) {
		this.newPayUserCount = newPayUserCount;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public Double getPaySum() {
		return paySum;
	}

	public void setPaySum(Double paySum) {
		this.paySum = paySum;
	}

	public Double getPayRate() {
		return payRate;
	}

	public void setPayRate(Double payRate) {
		this.payRate = payRate;
	}

	public Double getArppdau() {
		return arppdau;
	}

	public void setArppdau(Double arppdau) {
		this.arppdau = arppdau;
	}

	public Double getArpdau() {
		return arpdau;
	}

	public void setArpdau(Double arpdau) {
		this.arpdau = arpdau;
	}

}
