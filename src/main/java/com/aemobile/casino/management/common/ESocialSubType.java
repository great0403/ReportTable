/**
 * 
 */
package com.aemobile.casino.management.common;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Richard
 * 
 */
public enum ESocialSubType {

	FRIEND_ADD(1, 1), FRIEND_BEEN_ADD(1, 2), FRIEND_BLOCK(1, 3), FRIEND_BEEN_BLOCK(1, 4);

	private int type;
	private int code;

	private ESocialSubType(int type, int code) {
		this.type = type;
		this.code = code;
	}

	public int getType() {
		return type;
	}

	public int getCode() {
		return code;
	}

	public static List<ESocialSubType> getSubTypeList(int type) {
		List<ESocialSubType> list = new ArrayList<ESocialSubType>();
		for(ESocialSubType subType : ESocialSubType.values()) {
			if (subType.getType() == type) {
				list.add(subType);
			}
		}
		return list;
	}
}
