/**
 * 
 */
package com.aemobile.casino.management.common;

/**
 * @author Richard
 *
 */
public final class ErrorMessage {

	public static final String LOGIN_FAIL = "Please check your username and password and try again.";
}
