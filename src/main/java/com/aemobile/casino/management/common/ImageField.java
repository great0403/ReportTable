/**
 * 
 */
package com.aemobile.casino.management.common;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * icon或image字段上的注解，用于po向dto转换时自动拼上域名
 * 
 */
@Target({ METHOD, FIELD })
@Retention(RUNTIME)
public @interface ImageField {

	EImageType value() default EImageType.STATIC_DATA;

}
