/**
 * 
 */
package com.aemobile.casino.management.common;

/**
 * @author Richard
 * 
 */
public final class Permission {

	// user info
	public static final String VIEW_USER_INFO = "operation:userinfo:view";
	public static final String EDIT_USER_INFO = "operation:userinfo:edit";

	// game config
	public static final String VIEW_GAME_CONFIG = "operation:gameconfig:view";
	public static final String EDIT_GAME_CONFIG = "operation:gameconfig:edit";

	// game resource
	public static final String VIEW_GAME_RESOURCE = "operation:gameresource:view";
	public static final String EDIT_GAME_RESOURCE = "operation:gameresource:edit";

	// solt activity
	public static final String VIEW_SLOT_ACTIVITY = "operation:slotactivity:view";
	public static final String EDIT_SLOT_ACTIVITY = "operation:slotactivity:edit";

	// solt machine
	public static final String VIEW_SLOT_MACHINE = "operation:slotmachine:view";
	public static final String EDIT_SLOT_MACHINE = "operation:slotmachine:edit";

	// solt resource
	public static final String VIEW_SLOT_RESOURCE = "operation:slotresource:view";
	public static final String EDIT_SLOT_RESOURCE = "operation:slotresource:edit";

	// solt tournament
	public static final String VIEW_SLOT_TOURNAMENT = "operation:slottournament:view";
	public static final String EDIT_SLOT_TOURNAMENT = "operation:slottournament:edit";

	// advertisement
	public static final String VIEW_ADVERTISEMENT = "operation:advertisement:view";
	public static final String EDIT_ADVERTISEMENT = "operation:advertisement:edit";

	public static final String VIEW_ADSUSERIDS = "operation:adsuserids:view";
	public static final String EDIT_ADSUSERIDS = "operation:adsuserids:edit";

	// daily quest
	public static final String VIEW_DAILY_QUEST = "operation:dailyquest:view";
	public static final String EDIT_DAILY_QUEST = "operation:dailyquest:edit";

	// facebook activity
	public static final String VIEW_FB_ACTIVITY = "operation:fbactivity:view";
	public static final String EDIT_FB_ACTIVITY = "operation:fbactivity:edit";

	// online user
	public static final String LOAD_ONLINE_USER = "operation:onlineuser:load";
	public static final String REFRESH_ONLINE_USER = "operation:onlineuser:refresh";

	// user push
	public static final String VIEW_USER_PUSH = "operation:userpush:view";
	public static final String EDIT_USER_PUSH = "operation:userpush:edit";

	// integral wall
	public static final String VIEW_INTEGRAL_WALL = "operation:integralwall:view";
	public static final String EDIT_INTEGRAL_WALL = "operation:integralwall:edit";

	// announcement
	public static final String VIEW_ANNOUNCEMENT = "operation:announcement:view";
	public static final String EDIT_ANNOUNCEMENT = "operation:announcement:edit";

	// prize
	public static final String VIEW_PRIZE = "operation:prize:view";
	public static final String EDIT_PRIZE = "operation:prize:edit";

	// loyaltyPointPrize
	public static final String VIEW_LOYALTY_POINT_PRIZE = "operation:loyaltypointprize:view";
	public static final String EDIT_LOYALTY_POINT_PRIZE = "operation:loyaltypointprize:edit";

	// loyaltyPointExchange
	public static final String VIEW_LOYALTY_POINT_EXCHANGE = "operation:loyaltypointexchange:view";
	public static final String EDIT_LOYALTY_POINT_EXCHANGE = "operation:loyaltypointexchange:edit";

	// pushkey
	public static final String VIEW_PUSHKEY = "operation:pushkey:view";
	
	// userdetail
	public static final String VIEW_USER_DETAIL = "operation:userdetail:view";
	public static final String EDIT_USER_DETAIL = "operation:userdetail:edit";

	// userex change
	public static final String VIEW_USER_EXCHANGE = "operation:prize:view";
	public static final String EDIT_USER_EXCHANGE = "operation:prize:edit";

	// treasure
	public static final String VIEW_TREASURE = "operation:treasure:view";
	public static final String EDIT_TREASURE = "operation:treasure:edit";

	// server config
	public static final String VIEW_SERVER_CONFIG = "operation:serverconfig:view";
	public static final String EDIT_SERVER_CONFIG = "operation:serverconfig:edit";

	// server monitor
	public static final String VIEW_SERVER_MONITOR = "operation:servermonitor:view";

	// reload
	public static final String RELOAD = "operation:reload:edit";

	// sfs
	public static final String VIEW_SFS = "online:sfs:view";
	public static final String EDIT_SFS = "online:sfs:edit";

	// cardbook
	public static final String VIEW_CARDBOOK = "operation:cardbook:view";
	public static final String EDIT_CARDBOOK = "operation:cardbook:edit";

	// cardgroup
	public static final String VIEW_CARDGROUP = "operation:cardgroup:view";
	public static final String EDIT_CARDGROUP = "operation:cardgroup:edit";

	// card
	public static final String VIEW_CARD = "operation:card:view";
	public static final String EDIT_CARD = "operation:card:edit";

	// card
	public static final String VIEW_USERCARD = "operation:usercard:view";
	public static final String EDIT_USERCARD = "operation:usercard:edit";

	//Redeem Code
	public static final String VIEW_REDEEMCODE = "operation:redeemcode:view";
	public static final String EDIT_REDEEMCODE = "operation:redeemcode:edit";
	
	
	//Redeem Code
	public static final String VIEW_PAYLOAD = "operation:payload:view";
	public static final String EDIT_PAYLOAD = "operation:payload:edit";

	public static final String VIEW_USERBLACK = "operation:userblack:view";
	public static final String EDIT_USERBLACK = "operation:userblack:edit";
}
