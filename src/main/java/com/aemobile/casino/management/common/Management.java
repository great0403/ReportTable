/**
 * 
 */
package com.aemobile.casino.management.common;

/**
 * @author Richard
 *
 */
public class Management {
	public static final int DEFAULT_PAGE_SIZE = 15;

	public static final String DEFAULT_PAGE_SIZE_STR = "15";
	public static final String DEFAULT_PAGE_STR = "1";
}
