package com.aemobile.casino.management.common;


public enum Etour {
	Mega("1", "Mega"), Executive("2", "Executive"), Hi_Lo("3", "Hi-Lo"), Sit_n_Go("4", "Sit n Go"),;

	private String value;
	private String desc;

	Etour(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static String getDescByValue(String value) {
		for (Etour select : Etour.values()) {
			if (select.value.equals(value)) {
				return select.getDesc();
			}
		}
		return "";
	}
}
