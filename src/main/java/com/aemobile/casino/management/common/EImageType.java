/**
 * 
 */
package com.aemobile.casino.management.common;

/**
 * 图片前缀枚举，用户将config.properties中域名拼到指定字段值前
 * 
 * @author Richard
 * 
 */
public enum EImageType {

	APPLICATION("image.url.application"), STATIC_DATA("image.url.static");

	EImageType(String key) {
		this.key = key;
	}

	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
