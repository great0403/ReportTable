package com.aemobile.casino.management.common;


public enum GameConfig {
	Chipsads("chipsads","Chips广告"),
	AEBaccarat_levels("AEBaccarat-levels","AEBaccarat等级"),
	AEBingo_levels("AEBingo-levels","AEBingo等级");
	
	private String value;
	private String desc;
	
	GameConfig(String value, String desc){
		this.value=value;
		this.desc=desc;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static String getDescByValue(String value) {
		for (GameConfig select : GameConfig.values()) {
			if (select.value.equals(value)) {
				return select.getDesc();
			}
		}
		return "";
	}
	
	
	
}
